const API_PATH = "/subscriber";
const STATUS_CODES = {
  OK: 200,
  CREATED: 201,
  NOT_FOUND: 404,
  BAD_REQUEST: 400,
  INTERNAL_SERVER_ERROR: 500
};
const APP_PORT = 3000;

module.exports = {
  API_PATH,
  APP_PORT,
  STATUS_CODES
};