/* Amplify Params - DO NOT EDIT
  ENV
  REGION
  STORAGE_SUBSCRIBERS_ARN
  STORAGE_SUBSCRIBERS_NAME
Amplify Params - DO NOT EDIT */

const AWS = require('aws-sdk');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const bodyParser = require('body-parser');
const express = require('express');
const { v4: uuidv4 } = require('uuid');

const { API_PATH, APP_PORT, STATUS_CODES } = require('./constants');

AWS.config.update({ region: process.env.TABLE_REGION });

// DB
const dynamodb = new AWS.DynamoDB.DocumentClient();

// Express app
const app = express();

// Middleware
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

// Routing
app.get(API_PATH, async (req, res) => {
  const { query: { subscriberID } } = req;
  if (subscriberID) {
    const params = {
      Key: { 'recordID': subscriberID },
      TableName: process.env.STORAGE_SUBSCRIBERS_NAME,
    };

    try {
      const { Item } = await dynamodb.get(params).promise();
      if (!Item) {
        res
          .status(STATUS_CODES.NOT_FOUND)
          .json({ body: `Account record with ID: <| ${subscriberID} |> does not exists.` });
      } else {
        res
          .status(STATUS_CODES.OK)
          .json({
            body: {
              id: Item.recordID,
              email: Item.email,
              firstName: Item['first-name'],
              lastName: Item['last-name']
            }
          });
      }
    } catch (error) {
      res
        .status(STATUS_CODES.INTERNAL_SERVER_ERROR)
        .json({ body: `Internal Server Error: ${error.message || error}.` });
    }
  } else {
    res
      .status(STATUS_CODES.BAD_REQUEST)
      .json({ body: `Please provide valid account record ID in order to fetch account data.` });
  }
});

app.post(API_PATH, async (req, res) => {
  const { body: { email, firstName, lastName } } = req;
  if (email && firstName && lastName) {
    const recordID = uuidv4();
    const params = {
      TableName: process.env.STORAGE_SUBSCRIBERS_NAME,
      Item: {
        recordID,
        email,
        'first-name': firstName,
        'last-name': lastName
      }
    };

    try {
      await dynamodb.put(params).promise();
      res
        .status(STATUS_CODES.CREATED)
        .json({ body: `Account with ID: <| ${recordID} |> was succesfully created.` });
    } catch (error) {
      res
        .status(STATUS_CODES.INTERNAL_SERVER_ERROR)
        .json({ body: `Internal Server Error: ${error.message || error}.` });
    }
  } else {
    res
      .status(STATUS_CODES.BAD_REQUEST)
      .json({ body: `Please provide valid account data in order to create new account.` });
  }
});

app.listen(APP_PORT, function () {
  console.log(`App started on port: ${APP_PORT}`);
});

module.exports = app;
