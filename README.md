# Subscribers
Application is live and hosted on AWS, so you may visit it with: [Application URL](https://d2z75w90bypt5g.cloudfront.net)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.1.

## Development server

Run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` or `npm run build` to build the project. The build artifacts will be stored in the `dist/subscribers` directory.
