import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAccountComponent } from './create-account/create-account.component';
import { GetAccountDataComponent } from './get-account-data/get-account-data.component';

const routes: Routes = [
  { path: 'create-account', component: CreateAccountComponent },
  { path: 'get-account-data', component: GetAccountDataComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
