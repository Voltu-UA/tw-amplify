import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RestAPI } from '@aws-amplify/api-rest';

interface ISubscriberFetchResponse {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
}

@Component({
  selector: 'app-get-account-data',
  templateUrl: './get-account-data.component.html',
  styleUrls: ['./get-account-data.component.scss']
})
export class GetAccountDataComponent implements OnInit {
  public title = 'subscribers';
  public apiError = false;
  public apiErrorMessage = ''
  public formError = false;
  public apiResponse!: ISubscriberFetchResponse;
  public getSubscriberForm!: FormGroup;

  constructor(private fb: FormBuilder) {}

  async ngOnInit(): Promise<void> {
    this.getSubscriberForm = this.fb.group({
      subscriberID: ['', Validators.required],
    });
  }

  get subscriberID() { return this.getSubscriberForm.get('subscriberID'); }


  public async onFetchSubscribers(): Promise<void> {
    try {
      if (this.getSubscriberForm.valid) {
        const { subscriberID } = this.getSubscriberForm.value;
        const { body } = await RestAPI.get(
          'subscribersapi',
          `/subscriber?subscriberID=${subscriberID}`,
          {}
        );
        this.apiResponse = body;
        this.apiError = false;
        this.formError = false;
        this.getSubscriberForm.reset();
      } else {
        this.formError = true;
      }
    } catch (error) {
      this.apiError = true;
      const { response: { data: { body } } } = error;
      this.apiErrorMessage = body || error.message || JSON.stringify(error);
      console.error(`Fetching subscriber data failed: ${this.apiErrorMessage}`);
    }
  }
}
