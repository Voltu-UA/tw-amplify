import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RestAPI } from '@aws-amplify/api-rest';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
  public title = 'subscribers';
  public apiResponse = '';
  public apiError = false;
  public formError = false;
  public emailPattern = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,63}';
  public createSubscriberForm!: FormGroup;

  constructor(private fb: FormBuilder) {}

  async ngOnInit(): Promise<void> {
    this.createSubscriberForm = this.fb.group({
      email: [
        '',
        [Validators.required, Validators.email, Validators.pattern(this.emailPattern)]
      ],
      firstName: [
        '',
        [Validators.required, Validators.minLength(1), Validators.maxLength(255)]
      ],
      lastName: [
        '',
        [Validators.required, Validators.minLength(1), Validators.maxLength(255)],
      ]
    });
  }

  get email() { return this.createSubscriberForm.get('email'); }
  get firstName() { return this.createSubscriberForm.get('firstName'); }
  get lastName() { return this.createSubscriberForm.get('lastName'); }

  public async onCreateSubscriber(): Promise<void> {
    try {
      if (this.createSubscriberForm.valid) {
        const requestParams = {
          body: this.createSubscriberForm.value
        };
        const { body } = await RestAPI.post(
          'subscribersapi',
          '/subscriber',
          requestParams
        );
        this.apiResponse = body;
        this.apiError = false;
        this.formError = false;
        this.createSubscriberForm.reset();
      } else {
        this.formError = true;
      }
    } catch (error) {
      const { response: { data: { body } } } = error;
      this.apiResponse = body || error.message || error;
      this.apiError = true;
      console.error(`Creating subscriber account failed: ${this.apiResponse}`);
    }
  }
}
